import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;

import org.apache.commons.io.FilenameUtils;

public class Main {

    private static String CLASS_FOLDER;

    public static void main(String[] args) {
        try {
            CLASS_FOLDER = args[0];

            File file = new File(CLASS_FOLDER);
            File[] files = file.listFiles();

            HashMap<String, Object> objects = new HashMap<String, Object>();
            for (File f : files){
                if(FilenameUtils.getExtension(f.getName()).equals("class")) {
                    objects.put(FilenameUtils.removeExtension(f.getName()), getClassFromFile(FilenameUtils.removeExtension(f.getName())).newInstance());
                }
            }

            generateXMLFields(objects);
            System.exit(0);
        }catch (Exception ex){
            ex.printStackTrace();
            System.exit(0);
        }
    }



    private static Class getClassFromFile(String fullClassName) {
        try {
            URLClassLoader loader = new URLClassLoader(new URL[]{
                    new URL("file:///" + CLASS_FOLDER)
            });
            return loader.loadClass(fullClassName);
        }catch (ClassNotFoundException ex){
            ex.printStackTrace();
            return null;
        }catch (MalformedURLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    private static void generateXMLFields(HashMap<String, Object> objects){
        try{
            FieldsMetadata fieldsMetadata = new FieldsMetadata(TemplateEngineKind.Freemarker.name());

            for (HashMap.Entry<String, Object> entry: objects.entrySet()){
                fieldsMetadata.load(entry.getKey(), entry.getValue().getClass(), true);
            }
            fieldsMetadata.saveXML(new FileOutputStream(new File("params.fields.xml")), true);

        }catch (XDocReportException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
