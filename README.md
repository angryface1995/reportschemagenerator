# Report schema generator via XDocReport library
In /target/ you can find executable jar file.   
When executing this jar, it looks for .class files in specified directory, then generates schema in execution directory.

## How to use
From cmd/terminal write this:
```
java -jar path-to-jar-file path-to-class-directory
```
Path to class directory can be like: C:/path/to/directory/ or C:\path\to\directory\